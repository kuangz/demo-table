import React from 'react'
import { connect } from 'react-redux'

const ToastLabel = ({ toast }) => (
    <div className={`alert alert-${toast.type}`}>{toast.message}</div>
)

export default connect(state => ({ toast: state.toast }))(ToastLabel)