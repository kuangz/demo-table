import React from 'react'
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table'
import { Button } from 'react-bootstrap'
import 'react-bootstrap-table/dist/react-bootstrap-table-all.min.css'
import ConfirmDeleteModal from './ConfirmDeleteModal'


export default class TableRemote extends React.Component {
    state = {
        showModal: false,
        performDeletion: null
    }

    closeModal = () => {
        this.setState({ showModal: false });
    }

    openModal = () => {
        this.setState({ showModal: true });
    }



    handleConfirmDeleteCustomer = (next, dropRowKeys) => {
        this.setState({ showModal: true, performDeletion: next })
    }

    render() {
        const { customers, onAddCustomer, onDeleteCustomer, onEditCustomer, openEditingModal } = this.props;
        const { showModal, performDeletion } = this.state;

        function editCustomerButtonFormatter(cell, row) {
            return (
                <Button onClick={() => openEditingModal(row)}>แก้ไข</Button>
            );
        }
        const cellEditProp = {
            mode: 'click'
        };

        return (<div>
            <h1>Customer</h1>
            <ConfirmDeleteModal closeModal={this.closeModal} showModal={showModal} performDeletion={performDeletion} />
            <BootstrapTable
                data={customers}
                remote={true}
                selectRow={{ mode: 'checkbox' }}
                insertRow
                deleteRow={true}
                cellEdit={cellEditProp}
                options={{
                    onDeleteRow: onDeleteCustomer,
                    onAddRow: onAddCustomer,
                    onCellEdit: onEditCustomer,
                    handleConfirmDeleteRow: this.handleConfirmDeleteCustomer
                }}
            >
                <TableHeaderColumn dataField='id' isKey={true} >Customer ID</TableHeaderColumn>
                <TableHeaderColumn dataField='name'>Customer Name </TableHeaderColumn>
                <TableHeaderColumn dataField="button" dataFormat={editCustomerButtonFormatter} editable={false}>Edit</TableHeaderColumn>
            </BootstrapTable>

        </div>)
    }
}