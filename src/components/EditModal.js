import React from 'react'
import { FormGroup, ControlLabel, FormControl, HelpBlock, Button, Modal } from 'react-bootstrap'

const FieldGroup = ({ id, label, help, ...props }) => {
    return (
        <FormGroup controlId={id}>
            <ControlLabel>{label}</ControlLabel>
            <FormControl {...props} />
            {help && <HelpBlock>{help}</HelpBlock>}
        </FormGroup>
    );
}

export default ({ msg, closeModal, isShow, customer, performEdition, onCustomerNameChange }) => {
    return (
        <div>
            <Modal show={isShow} onHide={closeModal}>
                <Modal.Header closeButton>
                    <Modal.Title>Edit Modal</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <FieldGroup
                        id="name"
                        type="text"
                        label="Customer Name"
                        placeholder="Enter text"
                        value={customer.name}
                        onChange={(event) => {
                            event.preventDefault()
                            onCustomerNameChange(event.target.value)
                        }}
                    />
                </Modal.Body>
                <Modal.Footer>
                    <Button onClick={performEdition}>Confirm</Button>
                </Modal.Footer>
            </Modal>
        </div>
    )

}

