import React from 'react'
import { Button, Modal } from 'react-bootstrap'
export default ({ msg, closeModal, showModal, performDeletion }) => {
  const confirmDelete = () => {
    performDeletion()
    closeModal()
  }

  return (
    <div>
      <Modal show={showModal} onHide={closeModal}>
        <Modal.Header closeButton>
          <Modal.Title>Modal heading</Modal.Title>
        </Modal.Header>
        <Modal.Body>

        </Modal.Body>
        <Modal.Footer>
          <Button onClick={confirmDelete}>Confirm</Button>
        </Modal.Footer>
      </Modal>
    </div>
  )

}