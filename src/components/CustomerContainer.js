import React from 'react'
import { connect } from 'react-redux'
import { fetchCustomers, addCustomer, deleteCustomer, editCustomer } from '../actions/customers'
import { showErrorToast } from '../actions/toast'
import ToastLabel from './ToastLabel'
import CustomerTable from './CustomerTable'
import EditModal from './EditModal'
class Customer extends React.Component {

    componentDidMount() {
        const { fetchCustomers } = this.props;
        fetchCustomers();
    }

    state = {
        selectedCustomer: { id: "", name: "" },
        showEditModal: false,
    }

    closeEditModal = () => {
        this.setState({ showEditModal: false });
    }

    openEditModal = () => {
        this.setState({ showEditModal: true });
    }

    render() {
        const { customers } = this.props;
        return (

            <div>
                <EditModal 
                    closeModal={this.closeEditModal} 
                    isShow={this.state.showEditModal} 
                    customer={this.state.selectedCustomer} 
                    performEdition={this.onEditCustomer}
                    onCustomerNameChange={ (name) =>this.onCustomerChange( {name} )}
                />
                <ToastLabel />
                <div style={{margin: 0.5}}>

                <CustomerTable 
                    customers={customers}
                    onDeleteCustomer={this.deleteCustomer}
                    onAddCustomer={this.addNewCustomer}
                    onEditCustomer={this.onCellEdit}
                    openEditingModal={this.openCustomerEditModal}
                />
                </div>
            </div>
        );
    }

    deleteCustomer = (row) => {
        const { customers, deleteCustomer, showErrorToast } = this.props
        row.forEach((ele) => {
            if (customers.some(customer => customer.id === ele))
                deleteCustomer(ele);
            else
                showErrorToast("Customer id is not found")
        })
    }

    addNewCustomer = (newCustomer) => {
        const { customers, addCustomer, showErrorToast } = this.props

        if (customers.some(customer => customer.id.toString() === newCustomer.id))
            showErrorToast("Customer id is duplicated")
        else
            addCustomer(newCustomer.id, newCustomer.name);

    }

    onCellEdit = (row, fieldName, value) => {
        const { customers, editCustomer, showErrorToast } = this.props

        if (customers.some(customer => customer.id === row.id))
            editCustomer(row.id, fieldName, value)
        else
            showErrorToast("Customer id is not found")
    }

    onEditCustomer = () => {
        const { customers, showErrorToast } = this.props

            console.log(this.state.selectedCustomer)
        // if (customers.some(customer => customer.id === this.state.selectedCustomer.id))
        // else
        //     showErrorToast("Customer id is not found")
    }

    openCustomerEditModal = (customer) => {
        this.setState({ ...this.state, selectedCustomer: { id: customer.id, name: customer.name } })
        this.openEditModal();
    }

    onCustomerChange(partialCustomer) {
        this.setState({ ...this.state, selectedCustomer: { ...this.state.selectedCustomer, ...partialCustomer } })
    }

}


const mapStateToProps = state => ({ customers: state.fetch.customers })
const mapDispatchToProps = ({ fetchCustomers, addCustomer, deleteCustomer, showErrorToast, editCustomer })

const connector = connect(mapStateToProps, mapDispatchToProps)

export default connector(Customer)