import fetchMock from 'fetch-mock'
import FakeRest from 'fakerest'

const data = {
    customers: [
        { id: 1, name: "Poldet" },
        { id: 2, name: "Pongsakorn" }
    ]
}

const server = new FakeRest.FetchServer('http://localhost:3000/api')
server.init(data)

fetchMock.mock('begin:/api', server.getHandler())

// fetchMock.mock('begin:/api', (res, ...rest) => new Promise((resolve, reject) => {
//     setTimeout(() => {
//         server.handle(res)
//             .then(result => resolve(result))
//             .catch(error => reject(error))
//     }, 1000)
// }))