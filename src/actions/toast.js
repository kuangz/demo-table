export const SHOW_TOAST = "SHOW_TOAST"

export const showErrorToast = message => ({
    type: SHOW_TOAST,
    class: "danger",
    message
})

export default {
    SHOW_TOAST,
    showErrorToast
}