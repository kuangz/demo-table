export const fetchCustomers = () => ({
    type: "FETCH_CUSTOMERS",
    meta: { fetch_id: 'customers' },
    $payload: {
        url: "/api/customers"
    }
})

export const addCustomer = (id, name) => dispatch => dispatch({
    type: "ADD_CUSTOMER",
    $payload: {
        url: '/api/customers',
        options: {
            method: "POST",
            body: JSON.stringify({
                name
            })
        },
        onResponse: (res) => { 
            if (res.status === 201) 
                dispatch(fetchCustomers()) 
        }
    }
})

export const editCustomer = (id, fieldName, value) => ({
    type: "EDIT_CUSTOMER",
    $payload: {
        url: `/api/customers/${id}`,
        options: {
            method: "PUT",
            body: JSON.stringify({ [fieldName]: value })
        }
    }
})

export const deleteCustomer = (id) => ({
    type: "DELETE_CUSTOMER",
    $payload: {
        url: `/api/customers/${id}`,
        options: {
            method: "DELETE",
        }
    }
})
