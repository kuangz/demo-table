import { combineReducers } from 'redux'
import customers from './customers'
import toast from './toast'
import fetch from './fetch'

export default combineReducers({
    customers,
    toast,
    fetch
})