export default (state = [], action) => {
    switch (action.type) {
        case "FETCH_CUSTOMERS_SUCCESS":
            return action.data;
        case "ADD_CUSTOMER_SUCCESS":
            return [...state, action.data]
        case "DELETE_CUSTOMER_SUCCESS":
            return state.filter(({ id }) => id !== action.data.id)
        case "EDIT_CUSTOMER_SUCCESS":
            return state.map(customer => customer.id === action.data.id ? action.data : customer)
        default:
            return state
    }
}