import actions from '../actions/toast'

export default (state = { message: null }, action) => {
    switch(action.type) {
        case actions.SHOW_TOAST:
            return { message: action.message, type: action.class }
        default:
            return state
    }
}