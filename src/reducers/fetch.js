export default (state = {}, action) => {
    if (action.type && action.type.endsWith('_SUCCESS') && action.meta && action.meta.fetch_id) {
        return { ...state, [action.meta.fetch_id]: action.data }
    }

    return state
}
