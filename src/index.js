import './index.css';
import 'bootstrap/dist/css/bootstrap.css'

import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import registerServiceWorker from './registerServiceWorker';
import { createStore, applyMiddleware } from 'redux'
import { Provider } from 'react-redux'
import reducer from './reducers'

import './backend'

// MARK: - Middlewares
import thunk from 'redux-thunk'
import logger from 'redux-logger'
import fetchMiddlewareCreator from 'redux-fetch-middleware'
import fetchMiddlewareConfig from './modules/fetchMiddlewareConfig'

const fetch = fetchMiddlewareCreator(fetchMiddlewareConfig)

const store = createStore(
    reducer,
    applyMiddleware(thunk, fetch, logger)
)

ReactDOM.render(
    <Provider store={store}>
        <App />
    </Provider>, 
    document.getElementById('root')
);
registerServiceWorker();
