import React from 'react';
import './App.css';
import CustomerContainer from './components/CustomerContainer'
const App = () => (
  <div className="container">
     <CustomerContainer /> 
  </div>
)

export default App;
