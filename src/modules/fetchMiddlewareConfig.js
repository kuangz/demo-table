export default {
    suffix: ['REQUEST', 'SUCCESS', 'FAILURE'],
    debug: true,
    responseType: 'json',
    fetchOptions: {
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        }
    }
}